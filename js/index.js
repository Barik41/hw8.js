// 1. Опишіть своїми словами що таке Document Object Model (DOM)

// DOM це репрезентація веб сторінки, об'єктна модель документа. Створюється, коли браузер завантажує сторінку.
//Модель DOM включає вміст сторінки та елементи.Через модель DOM JS може взаємодіяти з сторінкою.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// innerText показує весь текстовий вміст, тобто будь-який текст,записаний між відкриваючим і закриваючим тегом.
//Всі елементи HTML всередині innerText ігноруються, повертається лише їх внутрішній текст.
//innerHTML показує текст лише по одному елементу, причому елементи HTML не ігноруються.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// Звернутися можна через: getElementById(),getElementByTagName(),getElementByClassName(),
//querySelectorAll(), querySelector(). Найкраще звертатися до id елемента, тобто getElementById().

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

// 4) Отримати елементи `<li>` вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  
  
const paragraphItem = document.getElementsByTagName("p");
for(let i = 0; i < paragraphItem.length; i++) {
    paragraphItem[i].style.backgroundColor = '#ff0000';
}

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

const parentElem = optionsList.parentElement;
console.log(parentElem);

const nodes = [...optionsList.childNodes];
nodes.forEach(items => {console.log(items.nodeType); 
console.log(items.nodeName)});

const newTextParagraph = document.getElementById("testParagraph");
newTextParagraph.innerHTML = 'This is a paragraph';

const elements = document.querySelector('.main-header').childNodes;
console.log(elements)
for(let element of elements) {
    if (element.classList) {
        element.classList.add('nav-item');
    };
};

let removedClasses = [...document.getElementsByClassName('section-title')];
removedClasses.forEach(removedClass => {
    removedClass.classList.remove('section-title')
})
    
    


